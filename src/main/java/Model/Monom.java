package Model;
public class Monom {
	int exp;
	double coef;
	
	//for better readibility i could have implemented 2 types of coef
	//one integer (for addition, multiplication, subtraction, derivate
	//one float/ double (for division and Integrate)
	public Monom(double coef,int exp) {
		super();
		this.coef=coef;
		this.exp=exp;
	}
	
	
	
		int get_exp() {
		
		return this.exp;
	}
		double get_coef() {
		
		return this.coef;
	}
	void setcoef(double x) {
		
		this.coef=x;
	
	}
	void setexp(int y)
	{
		this.exp=y;
	}

}
