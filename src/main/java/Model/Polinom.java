package Model;
import java.util.ArrayList;


public class Polinom {
	public ArrayList<Monom> polinom =new ArrayList<Monom>();
	
	public Polinom(Monom monom)
	{
		this.polinom.add(monom);
	}

	public void Polinom(Monom monom)
	{
		this.polinom.add(monom);
	}
	public void addTerm(double coef, int exp) {
		if (polinom.size() > exp && polinom.get(exp) != null) {
			double coefExistent = polinom.get(exp).get_coef();
			polinom.set(exp, new Monom(coefExistent + coef, exp));
		} else {
			polinom.add(exp, new Monom(coef, exp));
		}
	}
//	Boolean verifpol(Monom monom)
//	{
//		for(Monom x:this.polinom)
//		{
//			if(x.get_coef()==monom.get_coef() && x.get_exp()==monom.get_exp())
//			{
//				return false;
//			}
//		}
//		return true;
//	}
 @Override public String toString() {
	String s = null;	
	 for(Monom monom:polinom) 
	 {
		 if(monom.get_exp()>=0)
		 {
		 if(s!= null)	 
		 {	
			 if(monom.get_coef()!=0) 
		 		
				 if(monom.get_coef()<0)
		 			{
		 				s=s+monom.get_coef()+"*x^"+monom.get_exp();
		 				//this is the case when we have negative coeficient so it won't show as "-+"
		 			}
				 else
				 {
					 s=s+"+"+monom.get_coef()+"*x^"+monom.get_exp();
					//we add "+" before each term after the 1st
				 }
		 }

		 else
		 {
			 if(monom.get_coef()!=0)
			 {
				 s=monom.get_coef()+"*x^"+monom.get_exp(); //this is the case when we introduce
				 										  //the 1st monom so we don't put "+"
			 }
			 
			 
		 }
		 }
	 }
	 
	 return s;
 }
 public Polinom Sum(Polinom pol) {
		Polinom rez=new Polinom(new Monom(0,0));
		rez.polinom.clear();
      int bool=0;//We will use this bool to keep track of monomials that were already added in the rez
		for(Monom x:this.polinom) {
			bool=0;
			for(Monom y:pol.polinom) {
				if(x.get_exp()==y.get_exp()) {
					bool=1;
					rez.polinom.add(new Monom(x.get_coef()+y.get_coef(),x.get_exp())); //(*)
				}
			}
		}
		//This is not the most eficient method but it works...
		
		for(Monom x:this.polinom) { //For each monomial in the 1st polynomial we check if the the monomial was a "free term"
			   bool=0;
		
			for(Monom y:rez.polinom) {
				
			if(x.get_exp()==y.get_exp()) {
					
					bool=1;
				}
			
			}
		     if(bool==0){
		    	 	rez.polinom.add(new Monom(x.get_coef(),x.get_exp()));  } 
		    	}
		for(Monom x:pol.polinom) { //For each monomial in the 2nd polynomial we check if the the monomial was a "free term"
			   bool=0;
		
			for(Monom y:rez.polinom) {
				
			if(x.get_exp()==y.get_exp()) {//we skip this term because it was already added at (*)
					
					bool=1;
				}
			
			}
		     if(bool==0){
		    	 	rez.polinom.add(new Monom(x.get_coef(),x.get_exp()));  } // we add the term that was "free"
		    	}
		return rez;

		}
		

// This is the first variant of addition that i tried to implement
//	for(int i=0; i<pol.polinom.size(); i++)
// 	{
// 		rez.addTerm(pol.polinom.get(i).get_coef(), pol.polinom.get(i).get_exp(	));
// 	}
	
// public Polinom adunare(Polinom pol) {
//	 	Polinom rez=new Polinom(new Monom(0,0));
//	 	for(Monom x:this.polinom)
//	 		for(Monom y:pol.polinom) {
//	 			if(x.get_exp()==y.get_exp())
//	 			{
//	 				Monom monom=new Monom(x.get_coef()+y.get_coef(),x.get_exp());
//	 				rez.Polinom(monom);
//	 			}
//	 			
//	 				
//	 		}
//	 	for(Monom x:pol.polinom)
//	 	{
//	 			
//	 		Monom monom=new Monom(x.get_coef(),x.get_exp());
//	 		if(verifpol(monom)) {
//	 			rez.Polinom(x);
//	 		}
//			
//	 	}
//	 	for(Monom x:this.polinom)
//	 	{	
//	 		Monom monom=new Monom(x.get_coef(),x.get_exp());
//	 		if(verifpol(monom)) {
//	 			rez.Polinom(x);
//	 		}
//			
//	 	}
//	 	
//	 	
//	 	return rez;
// }
//almost the same as addition with a few exceptions
public Polinom Substraction(Polinom pol) {
	Polinom rez=new Polinom(new Monom(0,0));
	rez.polinom.clear();
	int minus=0;
 int bool=0;//We will use this bool to keep track of monomials that were already added in the rez 
	for(Monom x:this.polinom) {
		bool=0;
		
		for(Monom y:pol.polinom) {
			if(x.get_exp()==y.get_exp()) {
				
				{
					bool=1;
					rez.polinom.add(new Monom(x.get_coef()+y.get_coef()*(-1),x.get_exp()));
					//we can consider substraction as x +(-1)y
				}
			}
		}
	}
	for(Monom x:pol.polinom) {
		   bool=0;
		   minus=0;
	
		for(Monom y:rez.polinom) {
			
		if(x.get_exp()==y.get_exp()) {
				
				bool=1;
			}
		
		
		if(x.get_coef()<y.get_coef())
		{
			minus=0;
		}
		}
	    if(bool==0 && minus==0){
	    	 	rez.polinom.add(new Monom(-x.get_coef(),x.get_exp()));  } 
	    	
		else
		{	
			if(bool==0){
    	 	rez.polinom.add(new Monom(-x.get_coef(),x.get_exp()));  } 
    	}
	}
		
	for(Monom x:this.polinom) {
		   bool=0;
	
		for(Monom y:rez.polinom) {
			
		if(x.get_exp()==y.get_exp()) {
				
				bool=1;
			}
		if(x.get_coef()<y.get_coef())
		{
			minus=1;
		}
		}
	     if(bool==0 && minus==0){
	    	 	rez.polinom.add(new Monom(-x.get_coef(),x.get_exp()));  } 
	     else
	    	 if(bool==0 && minus==1){
		    	 	rez.polinom.add(new Monom(x.get_coef(),x.get_exp()));  } 
	    	}
		
	return rez;

}
 
 
 //this op is pretty straight-forward: we take each coefficient and 
 //multiply it with the old exponent, and the new exponent will be the old exponent-1
public Polinom Derivate(Polinom pol) {
	Polinom rez=new Polinom(new Monom(0,0));
	for(Monom x:this.polinom)
	{
		Monom monom=(new Monom(x.get_coef()*x.get_exp(), x.get_exp()-1));
		rez.Polinom(monom);
	}
	return rez;
}
//This operation and division are the reason why i declared the coefficient as double(could also be float)
//We use the mathematical formula that a monomial ax^n will become after integration a/(n+1)^(n+1)
public Polinom Integrate(Polinom pol) {
	Polinom rez=new Polinom(new Monom(0,0));
	for(Monom x:this.polinom)
	{
		Monom monom=(new Monom(x.get_coef()/(x.get_exp()+1), x.get_exp()+1));
		rez.Polinom(monom);
	}
	return rez;
}

//For multiplication we simply multiply the coeficients from monomials and add the exponents
public Polinom Multiply(Polinom pol) {
	Polinom rez=new Polinom(new Monom(0,0));
	for(Monom x:this.polinom)
 		for(Monom y:pol.polinom) {
 				Monom monom=new Monom(x.get_coef()*y.get_coef(),x.get_exp()+y.get_exp());
 				rez.Polinom(monom);	
 		}
	return rez;
 }
}
	
